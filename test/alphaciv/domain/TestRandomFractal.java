package alphaciv.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TestRandomFractal {
	RandomWorld rw = new RandomWorld(); 
	
	@Test
	public void testIfTileNotNull() {
		for (int r = 0; r < rw.getWorld().length; r++) {
			for(int c = 0; c < rw.getWorld().length; c++) {
				assertNotNull(rw.getWorld()[r][c]);
			}
		}
 
	}
}
