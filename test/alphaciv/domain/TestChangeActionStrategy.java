package alphaciv.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TestChangeActionStrategy {
	private Game game; 
	@Before
	public void setUp() {
		WinnerStrategy winningStrat = new RedWinsAt3000BCStrategy();
		WorldAgingStrategy worldAgingStrat = new IncreaseAgeBy100YearsStrategy();
		AllUnitsActionStrategy allUnitsAction = new AllUnitsActionStrategyImpl(new ArcherActionStrategy(),
				   new DoNothingActionStrategy(),
				   new SettlerActionStrategy());
		WorldGeneratorImpl worldGen = new WorldGeneratorImpl(allUnitsAction);
		//setting up 16x16 tiles 
		//ocean at (1,0) and mountains at (2,2)
		worldGen.createOceans(new Position(1, 0));
		worldGen.createMountains(new Position(2, 2));
		//red archer at (2,0), blue legion at (3,2), and red settler at (4,3)
		worldGen.createUnit(new Position(2,0), GameConstants.ARCHER, Player.RED);
		worldGen.createUnit(new Position(3,2), GameConstants.LEGION, Player.BLUE);
		worldGen.createUnit(new Position(4, 3), GameConstants.SETTLER, Player.RED);
		
		//Red city at (1, 1), Blue city at (4,1);
		worldGen.createCityAt(new Position(1,1), Player.RED);
		worldGen.createCityAt(new Position(4, 1), Player.BLUE);
		
		game = new GameImpl(winningStrat, worldAgingStrat, worldGen);
	}
	
	@Test
	public void testChange() {
		Position p = new Position (2, 0);
		Unit u = game.getUnitAt(p); 
		assertEquals("Position (2,0) has an archer", GameConstants.ARCHER, u.getTypeString());
		assertEquals("Defensive strength of archer initially 3", 3, u.getDefensiveStrength());
				
		game.setActionStrategyForUnit(u.getTypeString(), new SettlerActionStrategy());
	
		game.performUnitActionAt(p);
		assertEquals("The defensive strength is not doubled", 3, u.getDefensiveStrength());
	}
}
