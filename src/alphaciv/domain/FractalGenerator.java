package alphaciv.domain;

/**
 * Interface for class that generates a 16x16 HotCiv map
 * and enable clients to retrieve terrain type for
 * each position in the map.
 */
public interface FractalGenerator {

/**
 * get a character that indicates the type of terrain
 * at the given position(row, column).
 * @param row the row of the position of interest
 * @param col the column of the position of interest
 * @return a character defining the type of terrain
 *         at the specified position. Five types of
 *         of terrain are possible: �.� is ocean,
 *         �o� is plain, �f� is forest, �h� is hill,
 *         �M� is mountain.
 */
public char getLandscapeAt( int row, int col );
}

