package alphaciv.domain;

import java.util.HashMap;

public interface AllUnitsActionStrategy {
	public UnitActionStrategy getStrategyFor(String unitType);
	public void setStrategyFor(String unitType, UnitActionStrategy strategyType);
	public UnitActionStrategy getUnitActionStrat(String unitType); 
}
