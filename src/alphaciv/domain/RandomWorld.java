package alphaciv.domain;

import java.util.HashMap;

public class RandomWorld implements WorldFactory {
	
	private FractalGenerator tg; 
	private Tile[][] world;

	public RandomWorld() {
		world = new Tile[16][16];
		tg = new ThirdPartyFractalGeneratorImpl();
		setUpFractalWorld(); 
	}

	@Override
	public Tile[][] getWorld() {
		return world; 
	}

	@Override
	public HashMap<Position, UnitImpl> getUnits() {
		return null;
	}

	@Override
	public HashMap<Position, CityImpl> getCities() {
		return null;
	}

	@Override
	public AllUnitsActionStrategy getAllUnitsActionStrategy() {
		return null;
	}
	
	public void setUpFractalWorld() {
		Position p; 
		for(int r = 0; r < 16; r++) {
			for (int c = 0; c < 16; c++) {
				p = new Position(r, c);
				char terrain = tg.getLandscapeAt(r, c);
				
				switch(terrain) {
				case '.': //oceans
					world[r][c] = new TileImpl(p, GameConstants.OCEANS);
					break; 
				case 'f': //forest
					world[r][c] = new TileImpl(p, GameConstants.FOREST);
					break;
				case 'h': //hills
					world[r][c] = new TileImpl(p, GameConstants.HILLS);
					break; 
				case 'M': //mountains
					world[r][c] = new TileImpl(p, GameConstants.MOUNTAINS);
					break;
				default: //plains or non specified 
					world[r][c] = new TileImpl(p, GameConstants.PLAINS);
					break;
				}
				
			}
		}
	}

}
