package alphaciv.domain;

import thirdparty.ThirdPartyFractalGenerator;

public class ThirdPartyFractalGeneratorImpl implements FractalGenerator {
	ThirdPartyFractalGenerator tf; 
	
	public ThirdPartyFractalGeneratorImpl() {
		tf = new ThirdPartyFractalGenerator();
	}
	
	@Override
	public char getLandscapeAt(int row, int col) {
		return tf.getLandscapeAt(row, col); 
	}
}
