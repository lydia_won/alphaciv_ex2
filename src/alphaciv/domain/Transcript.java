package alphaciv.domain;

import java.util.ArrayList;

public interface Transcript {
	public void addTranscript(Player player, String action, Position p, String change);
	public void addTranscript(String action);
	public void printTranscript();
	public ArrayList<String> getTranscript(); 
}
