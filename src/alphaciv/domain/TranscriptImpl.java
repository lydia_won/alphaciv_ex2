package alphaciv.domain;

import java.util.ArrayList;

public class TranscriptImpl implements Transcript {

	private ArrayList <String> transcript; 
	
	public TranscriptImpl() {
		transcript = new ArrayList <String>();
	}
	
	@Override
	public void addTranscript(Player player, String action, Position p, String change) {
		String summary = player + " ";
		String pos = "(" + p.getRow() + ", " + p.getColumn() + ")";

		switch(action) {
		case "create":
			summary += "created unit at "+ pos + ".";
			break;
		case "move":
			summary += "moves unit to " + pos + "."; 
			break; 
		case "changeP":
			summary += "changes production in city at " + pos + " to " + change + ".";
			break;
		case "changeWF": 
			summary += "changes work force in city at " + pos + " to " + change + ".";
			break;
		default:
			//do nothing;
			break; 
		}
		
		System.out.println(summary);
		transcript.add(summary);
		
	}

	@Override
	public void printTranscript() {
		for(int i = 0; i < transcript.size(); i++) {
			System.out.println(transcript.get(i));
		}
	}

	@Override
	public void addTranscript(String action) {
		transcript.add(action);
	}

	@Override
	public ArrayList<String> getTranscript() {
		return transcript;
	}
	
	

}
