package alphaciv.domain;

import java.util.HashMap;

public class AllUnitsActionStrategyImpl implements AllUnitsActionStrategy {
	private HashMap <String, UnitActionStrategy> actionStrat; 
	
	public AllUnitsActionStrategyImpl(UnitActionStrategy archerStrat, UnitActionStrategy legionStrat, UnitActionStrategy settlerStrat) {
		actionStrat = new HashMap<String, UnitActionStrategy>(); 
		actionStrat.put(GameConstants.ARCHER, archerStrat);
		actionStrat.put(GameConstants.LEGION, legionStrat);
		actionStrat.put(GameConstants.SETTLER, settlerStrat);
	}
	
	
	@Override
	public UnitActionStrategy getStrategyFor(String unitType) {	
		return actionStrat.get(unitType);
	}
	
	@Override
	public void setStrategyFor(String unitType, UnitActionStrategy actionStrategy) {
		actionStrat.replace(unitType, actionStrategy);
	}

	@Override
	public UnitActionStrategy getUnitActionStrat(String unitType) {
		return actionStrat.get(unitType); 
	}

}
